'''
import execjs
def RunJs1():
    def get_js():
      f = open("./js/1.js", 'r', encoding='UTF-8')
      line = f.readline()
      str = ''
      while line:
          str = str + line
          line = f.readline()
      return str
    jsstr = get_js()
    ctx = execjs.compile(jsstr)
    return ctx.call('Encrypt', '18396835445')

def RunJs2():
    f = open("./js/1.js", 'r', encoding='UTF-8').read()
    ctx=execjs.compile(f)
    return ctx.call('Encrypt', '18396835445')
print("1"+RunJs1())
print("2"+RunJs2())
'''
import requests
import execjs
import json
def RunJs(word):
    f = open("./js/QQ.js", 'r', encoding='UTF-8').read()
    ctx=execjs.compile(f)
    return ctx.call('getToken', word)
def get(uid):
    url = "https://oidb.tim.qq.com/v3/oidbinterface/oidb_0xc9e_3?sdkappid=39998&actype=22&t=0.2788394125554541&g_tk="
    data = '{"uint64_guaranteed_uin":'+uid+',"uint32_flag":1}'
    str = requests.post(url=url,data=data,).text
    str = json.loads(str)
    retn = str['ActionStatus']
    if retn != 'OK':
        return retn
    return RunJs(str['str_url'])

print(get('34563'))


